# MyUpdateApp

#### 介绍
Android应用内升级App

1.  网络使用的Okhttp   com.squareup.okhttp3:okhttp:3.12.0
2.  下载完安装apk文件  并做了适配
     * 1.Android N  做FileProvider适配       在AndroidManifest里写 provider
     * 2.Android O  做INSTALL PERMISSION的适配  在AndroidManifest里添加REQUEST_INSTALL_PACKAGES权限
3.  获取文件MD5的算法
4.  下载过程中cancel中断处理

