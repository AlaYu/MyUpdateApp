package com.wjy.myupdateapp.updater;

import com.wjy.myupdateapp.updater.net.INetManager;
import com.wjy.myupdateapp.updater.net.OkhttpNetManager;

/**
 * created by WangJinyong
 */
public class AppUpdater {

    //单例
    private static AppUpdater sInstance = new AppUpdater();

    //网络请求，下载的能力
    //okhttp,volley,httpclient,httpurlconn
    /**
     * 接口隔离  具体实现    可以随意使用哪一种网路请求，只要改变一下NetManager就可以了，
     * 这里使用的是OkhttpNetManager，如果使用其他网路请求，则将OkhttpNetManager改成其他网路请求
     *
     */
    private INetManager mNetManager = new OkhttpNetManager();

    public void setNetManager(INetManager manager){
        mNetManager = manager;
    }

    public INetManager getNetManager(){
        return mNetManager;
    }

    //对外提供一个static方法
    public static AppUpdater getInstance(){
        return sInstance;
    }
}
