package com.wjy.myupdateapp.updater.net;

import java.io.File;

/**
 * Created by wjy.
 * Date: 2019/11/19
 * Time: 11:20
 * Describe: 下载接口 回调
 */
public interface INetDownloadCallBack {

    void success(File apkFile);

    void progress(int progress);

    void failed(Throwable throwable);
}
