package com.wjy.myupdateapp.updater.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.wjy.myupdateapp.R;
import com.wjy.myupdateapp.updater.AppUpdater;
import com.wjy.myupdateapp.updater.bean.DownloadBean;
import com.wjy.myupdateapp.updater.net.INetDownloadCallBack;
import com.wjy.myupdateapp.updater.utils.AppUtils;

import java.io.File;

/**
 * Created by wjy.
 * Date: 2019/11/19
 * Time: 15:21
 * Describe: ${describe}
 */
public class UpdateVersionShowDialog extends DialogFragment {

    private static final String KEY_DOWNLOAD_BEAN = "download_bean";

    private DownloadBean downloadBean;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null){
            downloadBean = (DownloadBean) arguments.getSerializable(KEY_DOWNLOAD_BEAN);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_updater,container,false);
        bindEvents(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void bindEvents(View view) {
        TextView tv_title = view.findViewById(R.id.tv_title);
        TextView tv_content = view.findViewById(R.id.tv_content);
        final TextView tv_update = view.findViewById(R.id.tv_update);
        tv_title.setText(downloadBean.title);
        tv_content.setText(downloadBean.content);
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);//点完 将按钮设置为不可点，不然每次点击都会重复的下载
                final File targetFile = new File(getActivity().getCacheDir(),"target.apk");
                //4.下载
                AppUpdater.getInstance().getNetManager().download(downloadBean.url, targetFile, new INetDownloadCallBack() {
                    @Override
                    public void success(File apkFile) {
                        v.setEnabled(true);
                        //安装的代码
                        Log.e("tag","success="+apkFile.getAbsolutePath());
                        dismiss();

                        String fileMd5 = AppUtils.getFileMd5(targetFile);
                        Log.e("tag","fileMd5="+fileMd5);
                        if (fileMd5 != null && fileMd5.equals(downloadBean.md5)){
                            AppUtils.installApk(getActivity(),apkFile);
                        }else {
                            Toast.makeText(getActivity(),"md5 检测失败！",Toast.LENGTH_SHORT).show();
                        }

                        AppUtils.installApk(getActivity(),apkFile);
                    }

                    @Override
                    public void progress(int progress) {
                        //更新界面的代码
                        Log.e("tag","progress="+progress);
                        tv_update.setText(progress+"%");
                    }

                    @Override
                    public void failed(Throwable throwable) {
                        v.setEnabled(true);
                        Toast.makeText(getActivity(),"文件下载失败！",Toast.LENGTH_SHORT).show();
                    }
                },UpdateVersionShowDialog.this);
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.e("tag","onDismiss");
        AppUpdater.getInstance().getNetManager().cancel(this);
    }

    public static void show(FragmentActivity activity, DownloadBean bean){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_DOWNLOAD_BEAN,bean);
        UpdateVersionShowDialog dialog = new UpdateVersionShowDialog();
        dialog.setArguments(bundle);
        dialog.show(activity.getSupportFragmentManager(),"updateVersionShowDialog");
    }
}
