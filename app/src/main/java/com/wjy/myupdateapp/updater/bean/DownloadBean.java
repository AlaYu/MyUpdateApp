package com.wjy.myupdateapp.updater.bean;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by wjy.
 * Date: 2019/11/19
 * Time: 14:24
 * Describe: ${describe}
 */
public class DownloadBean implements Serializable {

    public String title;
    public String content;
    public String url;
    public String md5;
    public String versionCode;

    //Json数据解析成对象
    public static DownloadBean parse(String response) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            String title = jsonObject.optString("title");
            String content = jsonObject.optString("content");
            String url = jsonObject.optString("url");
            String md5 = jsonObject.optString("md5");
            String versionCode = jsonObject.optString("versionCode");

            DownloadBean downloadBean = new DownloadBean();
            downloadBean.title = title;
            downloadBean.content = content;
            downloadBean.url = url;
            downloadBean.md5 = md5;
            downloadBean.versionCode = versionCode;
            return downloadBean;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
