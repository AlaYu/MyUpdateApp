package com.wjy.myupdateapp.updater.net;

/**
 * Created by wjy.
 * Date: 2019/11/19
 * Time: 11:20
 * Describe: 网络请求  get
 */
public interface INetCallBack {

    void success(String response);

    void failed(Throwable throwable);
}
