package com.wjy.myupdateapp.updater.net;

import android.content.Context;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by wjy.
 * Date: 2019/11/19
 * Time: 11:17
 * Describe: 网络模块
 */
public interface INetManager {

    void get(String url,INetCallBack callBack,Object tag);

    void post(String url, RequestBody requestBody,INetCallBack callBack);

    void POST(Context context,String url, String requestBody, INetCallBack callBack);

    void upload(String url, RequestBody requestBody, INetCallBack callBack);

    void download(String url, File targetFile,INetDownloadCallBack callback,Object tag);

    void cancel(Object tag);
}
